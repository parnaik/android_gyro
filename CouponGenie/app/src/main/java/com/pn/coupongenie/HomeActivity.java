package com.pn.coupongenie;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pn.coupongenie.adapter.CouponAdapter;
import com.pn.coupongenie.model.Coupon;
import com.pn.coupongenie.model.Coupons;
import com.pn.coupongenie.ui.AddCouponActivity;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        Button addButton = (Button) findViewById(R.id.add_coupon_btn);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // start new activity for creating new coupon
                Intent intent = new Intent(HomeActivity.this, AddCouponActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String serverDateTimeFormat = "YYYY-MM-DD'T'HH:mmZ";
        String displayDateTimeFormat = "d MMM Y h:m a";

        DateTime currentDate = new DateTime();
        Log.i("JODA_TIME", currentDate.toString(serverDateTimeFormat));

        Log.i("JODA_TIME", currentDate.toString(displayDateTimeFormat));

//        String dateStringFromServer = "2015-01-04T13:33:+0530";
//        SimpleDateFormat format = new SimpleDateFormat(serverDateTimeFormat);
//
//
//        DateTime jodaDateFromServer = new DateTime(dateStringFromServer);
//        Log.i("JODA_TIME_", jodaDateFromServer.toString(displayDateTimeFormat));

//        try {
//            //Date dateFromServer = format.parse(dateStringFromServer);
//
//        } catch (ParseException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        //refreshCouponData();
    }

    private void refreshCouponData() {
        final ListView couponListView = (ListView) findViewById(R.id.coupon_listview);
        Log.d("VOLLEY_REQUEST", "Sending new volley request to json generator site");

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.json-generator.com/api/json/get/bOaTgSkorS?indent=2";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Print data
                Log.d("VOLLEY_RESPONSE", response);

                // parse data
                Gson gsonBuilder = new Gson();
                Coupons couponsServerList = gsonBuilder.fromJson(response,Coupons.class);

                for (Coupon coupon : couponsServerList.couponList) {
                    Log.i("GSON_DATA", coupon.companyName);
                }

                CouponAdapter couponAdapter = new CouponAdapter(HomeActivity.this, couponsServerList.couponList);
                couponListView.setAdapter(couponAdapter);

                Log.d("VOLLEY_GSON_PARSING", "Just parsed json using gson");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // print error
                Log.e("VOLLEY_ERROR", error.getMessage());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
