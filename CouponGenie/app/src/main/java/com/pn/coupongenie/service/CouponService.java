package com.pn.coupongenie.service;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.pn.coupongenie.model.Coupon;

/**
 * Created by parnaik on 12/14/14.
 */
public class CouponService {

    // DO-NOT use this variable directly. use getCouponsArray() method.
    private static List<Coupon> coupons;

    private static List<Coupon> getCouponsArray() {
        if (null == coupons) {
            coupons = new ArrayList<Coupon>();
        }
        return coupons;
    }

    /**
     * Store new coupon
     *
     * @param coupon
     */
    public static void addCoupon(Coupon coupon) {
        getCouponsArray().add(coupon);
        Log.i("CouponService", "added new coupon with description: " + coupon.getInfo());
    }

    /**
     * Method to return all saved coupons
     *
     * @return List of coupons
     */
    public static List<Coupon> getAllCoupons() {
        return getCouponsArray();
    }

    public static String[] getCouponType() {
        String[] couponType = {"Electronics", "Health Care", "Holiday", "Rentals"};
        return couponType;
    }
}
