package com.pn.coupongenie.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by parnaik on 12/14/14.
 */
public class Coupon {

    @SerializedName("company")
    public String companyName;

    @SerializedName("info")
    public String info;

    @SerializedName("code")
    public String code;

    @SerializedName("type")
    public String type;

    public Coupon () {
        // empty constructor for gson.
    }

    public Coupon(String companyName, String info, String code, String type) {
        this.companyName = companyName;
        this.info = info;
        this.code = code;
        this.type = type;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
