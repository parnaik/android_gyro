package com.pn.coupongenie.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by parnaik on 1/4/15.
 */
public class Coupons {

    @SerializedName("couponlist")
    public List<Coupon> couponList;
}
