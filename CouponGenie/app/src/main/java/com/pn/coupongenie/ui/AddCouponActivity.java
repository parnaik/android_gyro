package com.pn.coupongenie.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.pn.coupongenie.R;
import com.pn.coupongenie.model.Coupon;
import com.pn.coupongenie.service.CouponService;

/**
 * Created by parnaik on 12/13/14.
 */
public class AddCouponActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_coupon_activity);
        // TODO - written to previous activity on save
        Button saveButtom = (Button) findViewById(R.id.save_coupon_btn);
        saveButtom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCouponInfo();
                finish();
            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.type_spinner);
        ArrayAdapter<CharSequence> adapter =
            new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_dropdown_item,
                CouponService.getCouponType());
        spinner.setAdapter(adapter);
    }

    /**
     * Helper method to save coupon information to coupon service.
     */
    private void saveCouponInfo() {

        EditText companyEditText = (EditText) findViewById(R.id.cupon_company_edittxt);
        EditText infoEditText = (EditText) findViewById(R.id.coupon_info_edittxt);
        EditText codeEditText = (EditText) findViewById(R.id.coupon_code_edittxt);
        Spinner typeSpinner = (Spinner) findViewById(R.id.type_spinner);

        Coupon coupon =
            new Coupon(companyEditText.getText().toString(), infoEditText.getText().toString(), codeEditText.getText()
                .toString(), typeSpinner.getSelectedItem().toString());
        CouponService.addCoupon(coupon);
    }
}
