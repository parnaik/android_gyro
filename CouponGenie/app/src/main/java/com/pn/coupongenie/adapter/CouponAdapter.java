package com.pn.coupongenie.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pn.coupongenie.R;
import com.pn.coupongenie.model.Coupon;

/**
 * Created by parnaik on 12/28/14.
 */
public class CouponAdapter extends ArrayAdapter<Coupon> {

    public CouponAdapter(Context context, List<Coupon> coupons) {
        super(context, 0, coupons);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.coupon_list_item, null);

        TextView companyTV = (TextView) v.findViewById(R.id.coupon_company_textview);
        TextView infoTV = (TextView) v.findViewById(R.id.coupon_info_textview);
        TextView codeTV = (TextView) v.findViewById(R.id.coupon_code_textview);
        TextView typeTV = (TextView) v.findViewById(R.id.coupon_type_textview);
        Coupon currentCoupon = getItem(position);

        companyTV.setText(currentCoupon.getCompanyName());
        infoTV.setText(currentCoupon.getInfo());
        codeTV.setText(currentCoupon.getCode());
        typeTV.setText(currentCoupon.getType());

        return v;
    }
}
