package com.pn.layout_example;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.TextView;

/**
 * Created by parnaik on 11/22/14.
 */
public class DetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        TextView firstNameTextView = (TextView) findViewById(R.id.detail_firstname);
        Bundle extras = getIntent().getExtras();
        String firstName = extras.getString("FIRST_NAME");

        firstNameTextView.setText(firstName);

    }
}