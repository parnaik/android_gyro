package com.pn.listexample.model;

/**
 * Created by parnaik on 11/30/14.
 */
public class Player {

    private String name;
    private String country;
    private String phone;
    private int age;

    public Player(String name, String country, int age) {
        this.name = name;
        this.country = country;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAge(int age) {

        this.age = age;
    }
}
