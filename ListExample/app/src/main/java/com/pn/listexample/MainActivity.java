package com.pn.listexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.pn.listexample.adapter.PlayerAdapter;
import com.pn.listexample.model.Player;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView playerListView = (ListView) findViewById(R.id.list_player);
        playerListView.setAdapter(new PlayerAdapter(this, 0, getPlayerList()));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<Player> getPlayerList() {
        List<Player> playerList = new ArrayList<Player>();
        playerList.add(new Player("Pele", "Brazil", 72));
        playerList.add(new Player("Messi", "Arg", 30));
        playerList.add(new Player("Ronaldo", "Por", 26));
        playerList.add(new Player("Pele 2", "Brazil", 72));
        playerList.add(new Player("Messi 2", "Arg", 30));
        playerList.add(new Player("Ronaldo 2", "Por", 26));
        playerList.add(new Player("Pele 3", "Brazil", 72));
        playerList.add(new Player("Messi 3", "Arg", 30));
        playerList.add(new Player("Ronaldo 3", "Por", 26));
        return playerList;
    }
}
