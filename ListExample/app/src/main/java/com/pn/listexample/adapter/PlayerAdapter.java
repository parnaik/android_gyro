package com.pn.listexample.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pn.listexample.R;
import com.pn.listexample.model.Player;

import java.util.List;

/**
 * Created by parnaik on 11/30/14.
 */
public class PlayerAdapter extends ArrayAdapter<Player> {
    public PlayerAdapter(Context context, int resource, List<Player> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;

        rowView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.player_list_view, null);

        TextView countryTextView = (TextView) rowView.findViewById(R.id.lst_country_textview);
        TextView nameTextView = (TextView) rowView.findViewById(R.id.lst_name_textview);
        TextView ageTextView = (TextView) rowView.findViewById(R.id.lst_age_textview);

//        countryTextView.setText("ARG");
//        nameTextView.setText("Messi");
//        ageTextView.setText("30");

        countryTextView.setText(getItem(position).getCountry());
        nameTextView.setText(getItem(position).getName());
        ageTextView.setText(String.valueOf(getItem(position).getAge()));

        return rowView;
    }
}
