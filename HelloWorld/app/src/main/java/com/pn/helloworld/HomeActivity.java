package com.pn.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class HomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.d("ANDROID_TRAINING", "just created home activity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("ANDROID_TRAINING","We are in pause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ANDROID_TRAINING", "we just resumed");
    }
}
